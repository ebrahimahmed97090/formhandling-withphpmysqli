<?php
#database connection file
#server name
$server ="localhost";
#database server username
$DBUsername = "root";
#database password
$DBPassword = "";
#database name
$DBName = "form";
#function to start a connection
$conn = mysqli_connect($server, $DBUsername, $DBPassword,$DBName);
#to make sure that connected well
if(!$conn){
    die("connection failed". mysqli_connect_error());
}