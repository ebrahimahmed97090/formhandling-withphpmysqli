#php opening tag (closing tag is not required in pure php files (non html))
<?php

#if signup form name 'regin' triggered run this code remember that $_POST is post method in html form
if (isset($_POST['regin'])) {
#database connection config
    require 'conn.php';
#variables to store fetched data from the form
    $UserName = $_POST['uname'];
    $Email = $_POST['uemail'];
    $PassWord = $_POST['upassword'];
    $RPassWord = $_POST['urepassword'];
#check if empty
    if (empty($UserName) || empty($Email) || empty($PassWord) || empty($RPassWord)) {
        header("location:../index.php?erroe=emptyfields&uUname=" . $UserName . "&uemail=" . $Email);
#exit means stop script
        exit();
#filter_var is a string filtration method to validate emails and some other strings in server side
#preg_match is a regular expression execution method to validate strings /^[ means start a-z means from a to z A-Z means from A-Z 0-9 means from 0-9 *$/ means
#end
    } elseif (!filter_var($Email, FILTER_VALIDATE_EMAIL) && !preg_match("/^[a-zA-Z0-9]*$/", $UserName)) {
#header to redirect to a location after ? is a customized error message in url
        header("location:../index.php?error=emailuname");
        exit();
    } elseif (!filter_var($Email, FILTER_VALIDATE_EMAIL)) {
        header("location:../index.php?error=email=&uname=" . $UserName);
        exit();
    } elseif (!preg_match("/^[a-zA-Z0-9]*$/", $UserName)) {
        header("location:../index.php?error=uname=&uemail=" . $Email);
        exit();
# to test password match
    } elseif ($PassWord !== $RPassWord) {
# . is to concatenate
        header("location:../index.php?error=passwordsmatch&uname=" . $UserName . "&email=" . $Email);
        exit();
    }
#prepared sql statement to prevent sql injections
    else {
#to not define database vars in sql statement prepare it with ? as a placeholder
        $sql = "SELECT Uname FROM auth WHERE Uname=?";
#initialize it into the database
        $stmt = mysqli_stmt_init($conn);
#check it it's working in database?
        if (!mysqli_stmt_prepare($stmt, $sql)) {
            header("Location: ../index.php?error=sqlerror");
            exit();
        } else {
#now add parameters on place holders first param is your statement second one is it's types s stand for one string
#if string and int si if two strings and int ssi and params after it the variables you want insert data from to the database
            mysqli_stmt_bind_param($stmt, "s", $UserName);
#execute the statement in database
            mysqli_stmt_execute($stmt);
#Store results
            mysqli_stmt_store_result($stmt);
#to know how many rows have this username if more than 0 (in this case will be one) so it will gives an error usertaken
            $resultCheck = mysqli_stmt_num_rows($stmt);
            if ($resultCheck > 0) {
                header("Location: ../index.php?error=usertaken&uemail=" . $Email);
                exit();
            }
#if 0 so inserting data to a table with another sql prepared statement
            else {
                $sql = 'INSERT INTO auth (Uname , email, upassword) values (? ,? , ?)' ;
                $stmt = mysqli_stmt_init($conn);
#sql error handler
                if (!mysqli_stmt_prepare($stmt, $sql)) {
                    header("Location: ../index.php?error=sqlerror");
                    exit();
                } else {
#to encrypt a password
                    $hashedPwd = password_hash($PassWord, PASSWORD_DEFAULT);
#don't forget to send encrypted one with another data to the database
                    mysqli_stmt_bind_param($stmt,'sss', $UserName, $Email, $hashedPwd);
                    mysqli_stmt_execute($stmt);
                    header("Location: ../index.php?signup=success");
                    exit();

                }
            }
        }

    }
#actually mysqli automatically closes the connection but just to make sure
    mysqli_stmt_close($stmt);
    mysqli_close($conn);
}
#return to the home page
else{
    header("Location: ../index.php");
}