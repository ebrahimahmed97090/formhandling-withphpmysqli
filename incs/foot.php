<!--this file must included at the end of every html file-->

<script src="Js/jquery-3.4.1.min.js"></script>
<!--popular javascript framework to make its syntax easier and required to run bootstrap script -->

<script src="Js/popper.min.js"></script>
<!--javascript framework required to run bootstrap script -->

<script src="Js/app.js"></script>
<!--empty javascript file will be needed in the future-->

<script src="fontawesome-pro-5.11.2-web/js/all.min.js"></script>
<!--fontawesome framework to include icons if needed-->

<script src="Js/bootstrap.bundle.min.js"></script>
<!--bootstrap script to ease of styling and laying out-->

</body>
</html>
<!--closing tags -->
